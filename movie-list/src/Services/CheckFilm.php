<?php

namespace App\Services;

class CheckFilm {

    private $apiKey;

    function __construct($apiKey){
        $this->apiKey = $apiKey;
    }

    public function isReal($name) {
        $name = str_replace(" ", "+", $name);

        $url = "http://www.omdbapi.com/?apikey=" . $this->apiKey . "&t=" . $name;

        $response = file_get_contents($url);

        return json_decode($response, true)["Response"];
    }

    public function getPlot($name){
        $name = str_replace(" ", "+", $name);

        $url = "http://www.omdbapi.com/?apikey=" . $this->apiKey . "&t=" . $name;

        $response = file_get_contents($url);

        return json_decode($response, true)["Plot"];
    }

    public function getFullName($name){
        $name = str_replace(" ", "+", $name);

        $url = "http://www.omdbapi.com/?apikey=" . $this->apiKey . "&t=" . $name;

        $response = file_get_contents($url);

        return json_decode($response, true)["Title"];
    }

}