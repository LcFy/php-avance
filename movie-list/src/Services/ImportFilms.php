<?php

namespace App\Services;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ImportFilms {
    private $serializer;

    function __construct(Array $encoders)
    { 
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    public function extractDataFrom($file, $type, Array $options = null)
    {
        $data = $this->serializer->decode($file, $type, $options);

        return($data);
    }
}