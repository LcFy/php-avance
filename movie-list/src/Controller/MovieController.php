<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Movie;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Expr\Cast\Array_;
use App\Form\AddType;
use App\Form\DeleteType;
use App\Form\ImportType;
use App\Form\SortType;
use App\Services\CheckFilm;
use App\Services\ImportFilms;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class MovieController extends AbstractController
{
    #[Route('/', name: 'movie')]
    public function index(EntityManagerInterface $em, Request $req): Response
    {
        $sortNbr = 1;
        $repo = $em->getRepository(Movie::class);
        $sorting = array('score' => 'DESC', 'name' => 'ASC');

        $form = $this->createForm(SortType::class);
        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid()) {
            $sortNbr = $form->getData()["sort"];
        }

        switch ($sortNbr) {
            case 1:
                break;
            case 2:
                $sorting = array('score' => 'ASC', 'name' => 'ASC');
                break;
            case 3:
                $sorting = array('name' => 'ASC', 'score' => 'DESC');
                break;
            case 4:
                $sorting = array('name' => 'DESC', 'score' => 'DESC');
                break;
        }

        $movies = $repo->findBy(array(), $sorting);
        //$movies = $repo->findAll();

        return $this->render('movie/index.html.twig', [
            'movies' => $movies,
            'route' => '/',
            'form' => $form->createView(),
            'sort' => $sortNbr
        ]);
    }

    #[Route('/display/{movieID}', name: 'display')]
    public function display($movieID, EntityManagerInterface $em): Response
    {
        $repo = $em->getRepository(Movie::class);

        $movie = $repo->find($movieID);

        return $this->render('movie/display.html.twig', [
            'movie' => $movie,
            'route' => ''
        ]);
    }

    #[Route('/add', name: 'add')]
    public function add(EntityManagerInterface $em, Request $req): Response
    {
        $added = false;
        $error = null;
        $movie = new Movie();
        $form = $this->createForm(AddType::class, $movie);
        $api = new CheckFilm("248e1b37");

        $form->handleRequest($req);
        if ($form->isSubmitted() && $form->isValid()){
            if($api->isReal($movie->getName()) == "True"){
                if($movie->getScore() >= 0 && $movie->getScore() <= 10){
                    $fullName = $api->getFullName($movie->getName());
                    $description = $api->getPlot($fullName);
                    $movie->setName($fullName);
                    $movie->setDescription($description);
                    $movie->setVotersNumber(1);
                    $em->persist($movie);
                    $em->flush();
                    $added = true;
                }
                else {
                    $error = "Le score doit-être compris entre 0 et 10";
                }
            }
            else {
                $error = "Film inconnu";
            }
        }

        return $this->render('movie/add.html.twig', [
            'form' => $form->createView(),
            'success' => $added,
            'error' => $error,
            'route' => '/add'
        ]);
    }



    #[Route('/delete/{movieID}', name: 'delete')]
    public function delete($movieID, EntityManagerInterface $em, Request $req): Response
    {
        $deleted = false;
        $error = null;
        $password = "";
        $form = $this->createForm(DeleteType::class);
        $repo = $em->getRepository(Movie::class);
        $movie = $repo->find($movieID);

        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid() && $form->getData()["password"] == "123456789"){
            //$movieRef = $em->getReference('App\Entity\Movie', $movieID);
            $em->remove($movie);
            $em->flush();
            $deleted = true;
        }
        elseif($form->isSubmitted()) {
            $error = "Mauvais mot de passe";
        }

        return $this->render('movie/delete.html.twig', [
            'movie' => $movie,
            'form' => $form->createView(),
            'success' => $deleted,
            'error' => $error,
            'route' => ''
        ]);
    }

    #[Route('/import', name: 'import')]
    public function import(EntityManagerInterface $em, Request $req): Response
    {
        $form = $this->createForm(ImportType::class);
        $count = null;
        $error = null;

        $form->handleRequest($req);
        if($form->isSubmitted() && $form->isValid()){

            $file = $form->get('file')->getData();
            $fileName = $file->getClientOriginalName();
            $ext = substr(strrchr($fileName, '.'), 1);

            if($ext == 'csv'){

                $csv = file_get_contents($file);

                $encoders = [new CsvEncoder()];

                $importer = new ImportFilms($encoders);

                $data = $importer->extractDataFrom($csv, 'csv', ['csv_delimiter' => ';']);

                $api = new CheckFilm("248e1b37");

                $count = 0;
                
                foreach($data as $item){
                    $movie = new Movie();
                    $movie->setName($item["name"]);
                    $movie->setDescription($item["description"]);
                    $movie->setScore($item["score"]);
                    if($api->isReal($movie->getName()) == "True"){

                        if($movie->getScore() >= 0 && $movie->getScore() <= 10){
                            $fullName = $api->getFullName($movie->getName());
                            $description = $api->getPlot($fullName);
                            $movie->setName($fullName);
                            $movie->setDescription($description);
                            
                            $em->persist($movie);
                            $count++;
                        }
                        else {
                            $error = "Tous les champs n'ont pas pu être ajoutés";
                        }
                    }
                    else {
                        $error = "Tous les champs n'ont pas pu être ajoutés";
                    }
                    
                }

                $em->flush();

            }
            else {
                $error = "Seul les fichiers .csv sont acceptés";
            }
            
        }

        return $this->render('movie/import.html.twig', [
            'form' => $form->createView(),
            'count' => $count,
            'error' => $error,
            'route' => '/import'
        ]);
    }
}
