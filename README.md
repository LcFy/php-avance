## Mot de passe admin
123456789  

## Ajout de la base de données
Nom de la bdd : movie_list  
Il suffit ensuite d'installer la migration après avoir créé la bdd  

## Fonctionnalitées
Le projet implémente les fonctionnalités principales suivantes :  
    - Ajout de film avec vérification de l'existance du film et ajout de la description via l'API  
    - Suppression de film avec mot de passe admin obligatoire  
    - Import de films via un fichier csv avec vérification similaire à l'ajout de film  
    - Affichage des films avec possibilité de tris par ordre alphabetique et notes  
    - Affichage des informations d'un film  